 var results = getJsonSync('data/results.json');

var catagory = ['numbers','measures','shape and space','data handling', 'algebra'];
 
 var date = new Date();
    date.getTime();
	date.setHours(0,0,0,0);
	date -= 6*24*60*60*1000;
var results = getJsonSync('data/results.json');
const oneday = 24*60*60*1000;
var dateLabel = [];
var tdate = new Date(date);
var intdate;
for (var a=0 ; a<7 ; a++){
	if(typeof intdate !== 'undefined'){
	tdate = new Date(intdate);	
	}
	console.log(tdate);
	dateLabel.push(tdate.getDate()+'/'+(tdate.getMonth()+1));
	intdate = Date.parse(tdate) + oneday;
	console.log(intdate);	
}

var result = [];
var result2 = [];
var result3 = [];
for (var a=0 ; a<7 ; a++){
	result.push({"correct":0,"total":0});
}
for (var a=0 ; a<6 ; a++){
	result2.push({"correct":0,"total":0});
}
for (var a=0 ; a<5 ; a++){
	result3.push({"correct":0,"total":0});
}

console.log(cookieJson.username);

for (var i=0 ; i<results.length ; i++){
//console.log(results[i].user);
	if(results[i].user == cookieJson.username || cookieJson.username == 'admin'){	
	//console.log('in');
		var rdate = new Date(results[i].date);
		//console.log(rdate);
		//console.log(new Date(date));
		
		//chart 1
		if(rdate >= date){
			var j = Math.floor((rdate - date) / oneday);
			console.log(j);
			results[i].answers.forEach(function(answer) {
			if(answer.correct){
				result[j].correct++;
			}
			result[j].total++;
			});
			
		}
//chart 2

			
			results[i].answers.forEach(function(answer) {
			var l = answer.level - 1;
			if(answer.correct){
				result2[l].correct++;
			}
			result2[l].total++;
			for (var key in catagory) {
		   if(answer.catagory == catagory[key]){
			   	if(answer.correct){
				result3[key].correct++;
			}
			result3[key].total++;
		   }
			}
			});
		
	}
}

//start report

var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
		labels: dateLabel,
				datasets: [{
					label: 'Total',
					backgroundColor: 'rgb(255,99,132)',
					borderColor: 'rgb(255,99,132)',
					data: [
						result[0].total,
						result[1].total,
						result[2].total,
						result[3].total,
						result[4].total,
						result[5].total,
						result[6].total
					],
					lineTension: 0,
					fill: false,
				}, {
					label: 'Correct',
					fill: false,
					backgroundColor: 'rgb(75, 192, 192)',
					borderColor: 'rgb(75, 192, 192)',
					data: [
						result[0].correct,
						result[1].correct,
						result[2].correct,
						result[3].correct,
						result[4].correct,
						result[5].correct,
						result[6].correct
					],
					lineTension: 0,
				}]
    },
    options: {
       responsive: false,
				title: {
					display: true,
					text: 'Past 7 days'
				},
				tooltips: {
					mode: 'index',
					intersect: false,
				},
				hover: {
					mode: 'nearest',
					intersect: true
				},
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'date'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Value'
						},
						ticks: {
						beginAtZero: true
						}
					}]
				}
			}
});

var catagory2 = catagory;
catagory2[2] = catagory[2].split(' ');
catagory2[3] = catagory[3].split(' ');

//rader

var ctx2 = document.getElementById("radar1").getContext("2d");
var myRader1 = new Chart(ctx2,  {
    type: 'radar',
    data: {
		        labels: [1,2,3,4,5,6],
        datasets: [{
					label: 'Total',
					backgroundColor: 'rgba(255,99,132,0.2)',
					borderColor: 'rgb(255,99,132)',
					pointBackgroundColor: 'rgb(255,99,132)',
					data: [
						result2[0].total,
						result2[1].total,
						result2[2].total,
						result2[3].total,
						result2[4].total,
						result2[5].total
					]
				}, {
					label: 'Correct',
					backgroundColor: 'rgba(75, 192, 192, 0.2)',
					borderColor: 'rgb(75, 192, 192)',
					pointBackgroundColor: 'rgb(75, 192, 192)',
					data: [
						result2[0].correct,
						result2[1].correct,
						result2[2].correct,
						result2[3].correct,
						result2[4].correct,
						result2[5].correct
					]
				}],

    },
    options: {
        responsive: false,
				title: {
					display: true,
					text: 'Level'
				}
    }
});

var ctx3 = document.getElementById("radar2").getContext("2d");
var myRader2 = new Chart(ctx3, {
    type: 'radar',
    data: {
		        labels: catagory2,
        datasets: [{
					label: 'Total',
					backgroundColor: 'rgba(255,99,132,0.2)',
					borderColor: 'rgb(255,99,132)',
					pointBackgroundColor: 'rgb(255,99,132)',
					data: [
						result3[0].total,
						result3[1].total,
						result3[2].total,
						result3[3].total,
						result3[4].total
					]
				}, {
					label: 'Correct',
					backgroundColor: 'rgba(75, 192, 192, 0.2)',
					borderColor: 'rgb(75, 192, 192)',
					pointBackgroundColor: 'rgb(75, 192, 192)',
					data: [
						result3[0].correct,
						result3[1].correct,
						result3[2].correct,
						result3[3].correct,
						result3[4].correct
					]
				}],

    },
    options: {
        responsive: false,
				title: {
					display: true,
					text: 'Catagory'
				}
    }
});
