	if(cookieJson.username == 'admin')
	window.location.href = './'; 


var app = new PIXI.Application(800, 600, {backgroundColor: 0x1099bb});
document.getElementById('canvas').appendChild(app.view);

var background = PIXI.Sprite.fromImage("skygrass.jpg");
background.width = app.screen.width;
background.height = app.screen.height;
app.stage.addChild(background);


//var product = getJsonSync('data/'+productUrl);

var question1 = getJsonSync('data/question.json');
var questions = question1.questions;
var questionList = [];
var question;
var weights = [];
var catagory = ['numbers','measures','shape and space','data handling', 'algebra'];

function flushLevel(){
	var results = getJsonSync('data/results.json');
	var rCorrect = 0;
	var rTotal = 0;
	var levelChange = false;
	var userResults = [];
	var j = 0;
	for (var i = 0; i < results.length; i++) {

  if(results[i].user == cookieJson.username){
  	results[i].answers.forEach(function(answer) {  		
	userResults[j] = answer;
	j++;	
	});

}
	}
	

	var userResultsStart = (userResults.length - 30) < 0 ? 0 : userResults.length - 30;
		
		for (var i = userResultsStart; i < userResults.length; i++) {

	if(userResults[i].level == level){
			if(userResults[i].correct){
				rCorrect++;
			}
			rTotal++;
			} else if(userResults[i].level > level){
				var levelDifference = userResults[i].level - level;
				if(userResults[i].correct){
				rCorrect += levelDifference;
				rTotal += levelDifference;
			}
			}
	}
for (var i = 0; i < users.length; i++) {
	if (cookieJson.username == users[i].user){
		console.log ('rc:'+rCorrect);
		console.log ('rt:'+rTotal);
	if(rCorrect/rTotal > 0.75 && rTotal > 10 && level < 6){
		users[i].level++;
		level = users[i].level;
		levelChange = true;
	} else if(rCorrect/rTotal < 0.2 && rTotal > 10 && level > 1){
		users[i].level--;
		level = users[i].level;
		levelChange = true;
	}
	}
}
console.log('l:'+level);

if(levelChange){
var postData = JSON.stringify(users);
$.ajax({
    url: 'file.php',
    type: "POST",
    data: {"path":"data/users.json","txt": postData},
    success: function(data, textStatus, jqXHR) {
        console.log('Success!');
    },
    error: function(jqXHR, textStatus, errorThrown) {
        console.log('Error occurred!');
    }
});
users = getJsonSync('data/users.json');
}
	
}

function weighQuestion(){
	var results = getJsonSync('data/results.json');

var wLevel = [];
for (var a=0 ; a<6 ; a++){
	wLevel.push(1);
}
for (var a=0 ; a<5 ; a++){
	weights.push(wLevel);
}
	
	flushLevel();
	
for (var i=0 ; i<results.length ; i++){
//console.log(results[i].user);
	if(results[i].user == cookieJson.username){	
	//console.log('in');
			
			results[i].answers.forEach(function(answer) {
			var l = answer.level - 1;
			for (var key in catagory) {
				//console.log("ll:"+l+","+level);
				if(l == level - 1)
				weights[key][l] += 3;
				
		   if(answer.catagory == catagory[key]){
			   	if(!answer.correct){
				weights[key][l]++;
			}
			
		   }
			}
			});
		
	}
}


	
}



function randomQuestion(){
	weighQuestion();
	var totalWeight = 0;



	var questionWeight = []
	
	for (var qid in questions){
		
		for (var cid in catagory){
			if (catagory[cid] == questions[qid].catagory){
				console.log(catagory[cid]);
				questionWeight.push(weights[cid][questions[qid].level-1]);
			}
		}
		
	}
	console.log(questionWeight);
	for (var w in questionWeight) {		
        totalWeight += questionWeight[w];		
    }
	//http://prosepoetrycode.potterpcs.net/2015/05/weighted-random-choices-js/
	while(questionList.length < 4){
var questionNumber = Math.floor(Math.random() * totalWeight);
console.log('qn:'+questionNumber);
var i = 0;
while (questionNumber > 0 && i < questionWeight.length){
questionNumber -= questionWeight[i];
i++;
}
i--;
if (!questionList.includes(i)){
	console.log('i:'+i);
	questionList.push(i);
}
}
}

randomQuestion();

function addResult(r){
	var results = getJsonSync('data/results.json');
	var date = new Date();
	r.date = date;
	results.push(r);
	var postData = JSON.stringify(results);

$.ajax({
    url: 'file.php',
    type: "POST",
    data: {"path":"data/results.json","txt": postData},
    success: function(data, textStatus, jqXHR) {
        console.log('Success!');
    },
    error: function(jqXHR, textStatus, errorThrown) {
        console.log('Error occurred!');
    }

});
}







var style1 = new PIXI.TextStyle({
fontFamily: "Comic Sans MS",
fontSize: 96,
align: 'center'
});

var style = new PIXI.TextStyle({
fontFamily: "Comic Sans MS",
fontSize: 32,
align: 'left',
wordWrap: true,
wordWrapWidth: app.screen.width-60
});

var styleA = new PIXI.TextStyle({
fontFamily: "Comic Sans MS",
fontSize: 48,
align: 'left',
wordWrap: true,
breakWords: true,
wordWrapWidth: 150
});

var title = new PIXI.Text('Quiz',style1);
title.x = app.screen.width * 0.3;
title.y = 0;

app.stage.addChild(title);
/*
var lorem = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';

var question = {
title:lorem,
choice:{
	A:"Lorem",
	B:"ipsum",
	C:"dolor",
	D:"sit"
},
answer:"C",
explanation:lorem
};
*/
var j = 0;

var score = 0;

//var basicText = new PIXI.Text(lorem,style);


var sprite = PIXI.Sprite.fromImage('next.svg');

// Set the initial position
sprite.anchor.set(1);
sprite.x = app.screen.width * 0.5;
sprite.y = app.screen.height * 0.8;

// Opt-in to interactivity
sprite.interactive = true;

// Shows hand cursor
sprite.buttonMode = true;

// Pointers normalize touch and mouse
sprite.on('pointerdown', onClick);

// Alternatively, use the mouse & touch events:
// sprite.on('click', onClick); // mouse-only
// sprite.on('tap', onClick); // touch-only

app.stage.addChild(sprite);

	var sprite2 = PIXI.Sprite.fromImage('return.svg');
	sprite2.anchor.set(1);
	sprite2.x = app.screen.width * 0.35;
	sprite2.y = app.screen.height * 0.8;
	sprite2.interactive = true;
sprite2.buttonMode = true;
sprite2.on('pointerdown', onClick);

var sprite4 = PIXI.Sprite.fromImage('tick.svg');
sprite4.anchor.set(1);
sprite4.height = app.screen.height * 0.25;
sprite4.width = sprite4.height;
sprite4.x = app.screen.width * 0.55;
	sprite4.y = app.screen.height * 0.75;
 //app.stage.addChild(sprite4);
	
var buttons = [];

var graphics,graphics3,basicText,basicText2;

var choices = ["A","B","C","D"];

var end = false;

var basicText3 = new PIXI.Text('',style);
	
var result = {"date":"","user":cookieJson.username,"answers":[]};	
	
function onClick () {
	if (j == 0){
	score = 0;	
	 graphics = new PIXI.Graphics();
graphics.lineStyle(2, 0xFFFF99, 1);
graphics.beginFill(0x66CC66, 1);
graphics.drawRoundedRect(30, 90, app.screen.width-60, 150, 15);
graphics.endFill();
	app.stage.addChild(graphics);
		graphics3 = new PIXI.Graphics();
graphics3.lineStyle(2, 0xFFFF99, 1);
graphics3.beginFill(0x66CC66, 1);
graphics3.drawRoundedRect(app.screen.width-160, 20, 150, 60, 15);
graphics3.endFill();
	app.stage.addChild(graphics3);
	basicText = new PIXI.Text('',style);
basicText.x = 30;
basicText.y = 90;
app.stage.addChild(basicText);
	basicText3.setText('Score:'+score);
	basicText3.x = app.screen.width-155;
	basicText3.y = 25;
	app.stage.addChild(basicText3);

app.stage.removeChild(sprite2);
	app.stage.removeChild(sprite);
	app.stage.removeChild(title);
	end = false;
	}
	else if (j == questionList.length && end){
		app.stage.removeChild(sprite);
		app.stage.addChild(sprite2);
		app.stage.removeChild(sprite4);
		result.score = score;
		addResult(result);
		result.answers = [];
		app.stage.removeChild(basicText2);
		basicText3.x = app.screen.width * 0.4;
		basicText3.y = app.screen.height / 2;	
		app.stage.removeChild(graphics);
		app.stage.removeChild(graphics3);
		app.stage.removeChild(basicText);
		j = 0;
		questionList = [];
		randomQuestion();
	}
	else{
app.stage.removeChild(sprite);
app.stage.removeChild(sprite4);	
app.stage.removeChild(basicText2);
	}
	if (!end){
for (var i = 0; i < 4; i++) {
var graphics2 = new PIXI.Graphics();
graphics2.lineStyle(2, 0xFFFF99, 1);
graphics2.beginFill(0x66CC66, 1);
graphics2.drawRoundedRect(i*160 + 30, app.screen.height-160 , 150, 150, 15);
graphics2.endFill();

question = questions[questionList[j]];

basicText.setText(question.title);


var answerText = choices[i]+': '+question.choice[choices[i]];
console.log(answerText);


    var button = graphics2;
    button.buttonMode = true;

    //button.anchor.set(0.5);
    //button.x = (i+1)*100;
    //button.y = 30;

    // make the button interactive...
    button.interactive = true;
    button.buttonMode = true;
	button.a = choices[i];
    button
        // Mouse & touch events are normalized into
        // the pointer* events for handling different
        // button events.
        .on('pointerdown', onButtonDown);

        // Use mouse-only events
        // .on('mousedown', onButtonDown)
        // .on('mouseup', onButtonUp)
        // .on('mouseupoutside', onButtonUp)
        // .on('mouseover', onButtonOver)
        // .on('mouseout', onButtonOut)

        // Use touch-only events
        // .on('touchstart', onButtonDown)
        // .on('touchend', onButtonUp)
        // .on('touchendoutside', onButtonUp)
	//button.setText("Play!", style);
    // add it to the stage
    app.stage.addChild(button);
	
button.text = new PIXI.Text(answerText,styleA);
button.text.x = i*160 + 30;
button.text.y = app.screen.height-160;
app.stage.addChild(button.text);


    // add button to array
    buttons.push(button);
	




		
	//buttons[i].on('pointerdown', onButtonDown);
	}
	}
}





function onButtonDown (){
	console.log(this.a);
	//var key = this.keys;
	//var basicText2 = new PIXI.Text(key,style);
	var aa = this.a;
	//console.log(aa);
	var answered = {
"question":question.id,
"level":question.level,
"catagory":question.catagory,
"answer":aa
};
	if(this.a == question.answer){	
	app.stage.addChild(sprite4);
	basicText2 = new PIXI.Text('Right!',style);
	basicText2.x = app.screen.width * 0.4;
	basicText2.y = app.screen.height * 0.75;
	score++;
	answered.correct = true;	
	}else{
	basicText2 = new PIXI.Text('Wrong! The correct answer is '+question.answer,style);
	basicText2.x = app.screen.width * 0.25;
	basicText2.y = app.screen.height / 2;
	answered.correct = false;
	}
	basicText3.setText('Score:'+score);
	console.log('Score:'+score);
app.stage.addChild(basicText2);
buttons.forEach(function(button){
	app.stage.removeChild(button.text);
	app.stage.removeChild(button);	
});
j++;
result.answers.push(answered);	
sprite.x = app.screen.width;
sprite.y = app.screen.height;
if(j == questionList.length)
	end = true;


app.stage.addChild(sprite);

}
//app.stage.addChild(sprite2);

/*
1.onButtonOver
2.onButtonOut
3.JSON array
4.random
5.Next button
6.Score
*/
