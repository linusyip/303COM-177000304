
var list = {"Home":{"active":"","link":"index.html","login":""},
"Lecture":{"active":"","link":"lecture.html","login":""},
"Quiz":{"active":"","link":"quiz.html","login":"required"},
"Report":{"active":"","link":"report.html","login":"required"}};

if(typeof list[realm] !== 'undefined')
list[realm].active = 'active';



function tagChange(el, tag){
    var parent = el.parentNode
       ,newElem = document.createElement(tag);
    newElem.innerHTML = el.innerHTML
    parent.replaceChild(newElem, el);
}

if (document.getElementById('nav') != null)
tagChange(document.getElementById('nav'), 'nav');

var toptype = realm == 'login' ? 'fixed-top' : 'sticky-top';

navbar = document.getElementsByTagName('nav')[0];
navbar.className = "navbar navbar-expand-sm "+toptype+" navbar-dark bg-dark";

var html = '<a class="navbar-brand" href="./"><img src="logo.svg" width="30" height="30" class="d-inline-block align-top" alt="">Learn Maths</a>';  
html += '<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">';
html += '<span class="navbar-toggler-icon"></span>';
html += '</button>';
html += ' <div class="collapse navbar-collapse" id="navbarNavDropdown">';
html += '<ul class="navbar-nav  mr-auto">';
Object.keys(list).forEach(function(k){
	if(typeof cookieJson.username !== 'undefined' || list[k].login == ''){
	var active = list[k].active == 'active' ? ' active': '';
	var active2 = list[k].active == 'active' ? ' <span class="sr-only">(current)</span>': '';
	var link = list[k].link;
	//var link = list[k].active == 'active' ? '#' : list[k].link;
	if(k != 'Quiz' || cookieJson.username != 'admin'){
	html += ' <li id="home" class="nav-item'+ active + '">';
	html += '<a class="nav-link" href="'+link+'">'+k+active2+'</a></li>';
	}
	}
});
html += '</ul>';
html += '<ul class="navbar-nav  ml-auto">';

var active = '';
if(typeof cookieJson.username !== 'undefined'){

	    html += '<li class="nav-item dropdown">';
        html += '<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
        html += realname;
        html += '</a>';
        html += '<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">';
        if (cookieJson.username == 'admin'){
		active = realm == 'users' ? " active" : "";
		html += '<a class="dropdown-item'+active+'" href="users.html">User list</a>';}
		active = realm == 'setting' ? " active" : "";		 
		html += '<a class="dropdown-item'+active+'" href="setting.html">Settings</a>';
        html += '<a class="dropdown-item" id="logout" href="#">Log out</a>';
        html += '</div>';
     
}
else{
	html += '<li class="nav-item">';
	if (realm == 'login')
		active = " active";
	
html += '<a class="nav-link'+active+'" href="login.html">Log in</a>';
}
html += '</li></ul></div>';
navbar.innerHTML = html;

if(document.getElementById('logout') != null)
document.getElementById('logout').onclick = logout;
