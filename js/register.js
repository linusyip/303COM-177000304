

function register(){
	
	var users = getJsonSync('data/users.json');
var overlap = false;

var name = $("#inputName").val().trim();
var user = $("#inputUser").val().toLowerCase().trim();
var password = $("#inputPassword").val();
var repassword = $("#inputRePassword").val();

	var valid = true; 
$("#inputName").css("border-color","");
$("#inputUser").css("border-color","");
$("#inputPassword").css("border-color","");
$("#inputRePassword").css("border-color","");
$("#check-invalid").text( "" ); 
if(name.length < 1){
$("#inputName").css("border-color","#dc3545");
$("#check-invalid").text( "Please input name." ); 
valid = false;
}
if(user.match(/^[0-9a-zA-Z]+$/) === null){
$("#inputUser").css("border-color","#dc3545");
valid = false;
if($("#check-invalid").text() == '')
$("#check-invalid").text( "Only letters and numbers are allowed for username." ); 	
}
var password2 = password.length >= 6 ;
if(!password2){
$("#inputPassword").css("border-color","#dc3545");
valid = false;	
if($("#check-invalid").text() == '')
$("#check-invalid").text( "Password length must at least 6." ); 
}
if(repassword != password || !password2){
$("#inputRePassword").css("border-color","#dc3545");
valid = false;
if($("#check-invalid").text() == '')
$("#check-invalid").text( "Password not matched." ); 	
}

if (valid){
for(var i=0;i<users.length;i++){	 
	 if (users[i].user == user){
 overlap = true;
 $("#check-invalid").text( "Please use another username." ); 	
	 break;
	 }
};
console.log(overlap);
if(!overlap){
users.push({"name":name,"user":user,"password":sha1(password),"level":1});

var postData = JSON.stringify(users);

$.ajax({
    url: 'file.php',
    type: "POST",
    data: {"path":"data/users.json","txt": postData},
    success: function(data, textStatus, jqXHR) {
        console.log('Success!');
    },
    error: function(jqXHR, textStatus, errorThrown) {
        console.log('Error occurred!');
    }

});

window.location.href = './success.html'; 

}
}
}

 $( "#register" ).on( "click", register );
 
