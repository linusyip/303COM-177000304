if(typeof loginRequired === 'undefined')
var loginRequired = false;

var users = getJsonSync('data/users.json');

function cookieToJson(){
var json = '{';
var split_read_cookie = document.cookie.split("; ");
for (i=0;i<split_read_cookie.length;i++){
    var value=split_read_cookie[i];
    value=value.split("=");
	json += '"' + value[0] +'":"'+ value[1]+ '"';
	if (split_read_cookie.length-i > 1){
	json += ',';
	}
}
json += '}'
return JSON.parse(json);
}

var cookieJson = cookieToJson();
var realname = '';
var level = 0;

function logout(){
document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC; ";
document.cookie = "password=; expires=Thu, 01 Jan 1970 00:00:00 UTC; ";
window.location.href = './'; 
}

if (typeof users !== 'undefined'){
	var valid = false;
	users.forEach(function(value) {
		if (cookieJson.username == value.user){
			if (cookieJson.password == value.password){
			valid = true;
			realname = value.name;
			level = value.level;
		}
		}
			//console.log(value.user);
			//console.log(value.password);
	});
	//console.log (cookieJson.username);

	//console.log (cookieJson.password);

	//console.log (valid);
	if(!valid && loginRequired){
		window.location.href = './login.html?realm='+realm.toLowerCase(); 
	}
}

if (typeof cookieJson.username !== 'undefined' && realm == 'login'){
	window.location.href = './'; 
}