var app = new PIXI.Application(800, 600, {backgroundColor: 0x1099bb});
document.getElementById('canvas').appendChild(app.view);

var background = PIXI.Sprite.fromImage("skygrass.jpg");
background.width = app.screen.width;
background.height = app.screen.height;
app.stage.addChild(background);

var text = ["This lesson is about the carry. A carry is a number which shows the overflow.","You can see the expression.","7 + 9 = 16. Therefore, 6 is the answer and 1 is the carry","Then, we need to add together with this carry.","Then 1 + 2 + 5 = 8 and there is no carry. Therefore, the answer is 86."];

var style1 = new PIXI.TextStyle({
fontFamily: "Comic Sans MS",
fontSize: 96,
align: 'center'
});

var style = new PIXI.TextStyle({
fontFamily: "Comic Sans MS",
fontSize: 32,
align: 'left',
wordWrap: true,
wordWrapWidth: 440
});

var title = new PIXI.Text('Carry',style1);
title.x = app.screen.width * 0.3;
title.y = 0;

var basicText = new PIXI.Text(text[0],style);
basicText.x = 30;
basicText.y = 90;

var style2 = new PIXI.TextStyle({
fontFamily: "Comic Sans MS",
fontSize: 48,
align: 'right',
wordWrap: true
});

var style3 = new PIXI.TextStyle({
fontFamily: "Comic Sans MS",
fontSize: 24,
fill: 'red',
align: 'right',
wordWrap: true
});

var aText = new PIXI.Text('\u2000\u200027',style2);
aText.x = app.screen.width * 0.4;
aText.y = app.screen.height * 0.5;

var bText = new PIXI.Text('+\u200059',style2);
bText.x = aText.x;
bText.y = aText.y + 48;

var underline = new PIXI.Text('____',style2);
underline.x = bText.x;
underline.y = bText.y + 6;

var cText = new PIXI.Text('\u2000\u2000\u20006',style2);
cText.x = aText.x;
cText.y = underline.y + 48;

var cText2 = new PIXI.Text('\u2000\u200086',style2);
cText2.x = aText.x;
cText2.y = underline.y + 48;

var carry = new PIXI.Text('',style2);


var graphics = new PIXI.Graphics();
graphics.lineStyle(2, 0xFFFF99, 1);
graphics.beginFill(0x66CC66, 1);
graphics.drawRoundedRect(30, 90, 450, 150, 15);
graphics.endFill();

// Scale mode for all textures, will retain pixelation
PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;

var pig = PIXI.Sprite.fromImage('pig.svg');
pig.x = app.screen.width * 0.6;
pig.y = app.screen.height * 0.2;



var sprite = PIXI.Sprite.fromImage('next.svg');

// Set the initial position
sprite.anchor.set(1);
sprite.x = app.screen.width * 0.5;
sprite.y = app.screen.height * 0.8;

// Opt-in to interactivity
sprite.interactive = true;

// Shows hand cursor
sprite.buttonMode = true;

// Pointers normalize touch and mouse
sprite.on('pointerdown', onClick);

// Alternatively, use the mouse & touch events:
// sprite.on('click', onClick); // mouse-only
// sprite.on('tap', onClick); // touch-only

app.stage.addChild(sprite);

	var sprite3 = PIXI.Sprite.fromImage('next.svg');
	sprite3.anchor.set(1);
	sprite3.x = app.screen.width;
	sprite3.y = app.screen.height;
	sprite3.interactive = true;
sprite3.buttonMode = true;
sprite3.on('pointerdown', onClick2);

	var sprite2 = PIXI.Sprite.fromImage('return.svg');
	sprite2.anchor.set(1);
	sprite2.x = app.screen.width * 0.35;
	sprite2.y = app.screen.height * 0.8;
	sprite2.interactive = true;
sprite2.buttonMode = true;
sprite2.on('pointerdown', onClick);

var i = 1;

var firstTime = true ;

app.stage.addChild(title);

function onClick () {
if(firstTime){
	app.stage.removeChild(title);
	app.stage.addChild(pig);
	app.stage.removeChild(sprite);
	sprite.x = app.screen.width;
	sprite.y = app.screen.height;
	app.stage.addChild(sprite);
	app.stage.addChild(graphics);
	firstTime = false ;
}
else{
app.stage.removeChild(basicText);
basicText = basicText2;
}

if(i == 1 && !firstTime)
{
	app.stage.removeChild(sprite2);
	app.stage.addChild(sprite);
	app.stage.removeChild(carry);
app.stage.removeChild(aText);
app.stage.removeChild(bText);
app.stage.removeChild(underline);
app.stage.removeChild(cText2);
}	
//console.log(i);
switch (i){
	case 2:
	app.stage.addChild(aText);
	app.stage.addChild(bText);
	app.stage.addChild(underline);
	break;
	case 3:
	carry.setText('\u2000\u20001');
	carry.setStyle(style2);
	carry.x = cText2.x;
	carry.y = cText2.y ;
	app.stage.addChild(carry);
	app.stage.addChild(cText);
	app.stage.removeChild(sprite);
	app.stage.addChild(sprite3);
	break;
	case 0:	
	app.stage.removeChild(cText);
	app.stage.addChild(cText2);
	break;
default:	
}
app.stage.addChild(basicText);
console.log(i+'a');

basicText2 = new PIXI.Text(text[i],style);
basicText2.x = 30;
basicText2.y = 90;

i++;
if (i == 1)
{
	app.stage.removeChild(sprite);
	app.stage.addChild(sprite2);
}
if (i == text.length)
{
	i = 0;	
}
if (i == 4)
i--;

//    sprite.scale.x *= 1.25;
//    sprite.scale.y *= 1.25;


}
function onClick2 () {
	console.log(i+'b');
	carry.setText('1');
	carry.setStyle(style3);
	carry.x = aText.x + 60;
carry.y = aText.y - 24 ;
i++;
	app.stage.removeChild(sprite3);
	app.stage.addChild(sprite);
}


